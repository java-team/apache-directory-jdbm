#!/bin/sh -e
#
# Removes unwanted content from the upstream sources.
# Called by uscan with '--upstream-version' <version> <file>
#

VERSION=$2
TAR=../apache-directory-jdbm_$VERSION.orig.tar.xz
DIR=apache-directory-jdbm-$VERSION
TAG=$(echo "$VERSION" | sed -re's/~(alpha|beta|rc|M)/-\1/')

svn export http://svn.apache.org/repos/asf/directory/jdbm/tags/${TAG}/ $DIR
XZ_OPT=--best tar -c -J -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
