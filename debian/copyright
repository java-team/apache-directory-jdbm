Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: ApacheDS JDBM
Source: http://svn.apache.org/repos/asf/directory/jdbm

Files: *
Copyright: 2000-2001, Alex Boisvert
           2000, Cees de Groot
License: JDBM

Files: jdbm1/src/main/java/jdbm/I18n.java
       jdbm1/src/main/resources/*
       jdbm1/src/site/*
       jdbm1/src/test/java/jdbm/recman/LocationTest.java
       jdbm2/src/main/java/jdbm/ActionRecordManager.java
       jdbm2/src/main/java/jdbm/I18n.java
       jdbm2/src/main/java/jdbm/helper/ActionContext.java
       jdbm2/src/main/java/jdbm/helper/ActionVersioning.java
       jdbm2/src/main/java/jdbm/helper/EntryIO.java
       jdbm2/src/main/java/jdbm/helper/ExplicitList.java
       jdbm2/src/main/java/jdbm/helper/LRUCache.java
       jdbm2/src/main/java/jdbm/recman/SnapshotRecordManager.java
       jdbm2/src/main/resources/*
       jdbm2/src/site/*
       jdbm2/src/test/java/jdbm/btree/TestBTreeBrowser.java
       jdbm2/src/test/java/jdbm/btree/TestSnapshotBTree.java
       jdbm2/src/test/java/jdbm/helper/*
       jdbm2/src/test/java/jdbm/recman/*
       jdbm2/src/test/resources/*
Copyright: 2012-2013, The Apache Software Foundation
License: Apache-2.0

Files: debian/*
Copyright: 2015-2023, Emmanuel Bourg <ebourg@apache.org>
           2017, Hans Joachim Desserud
           2023, tony mancill <tmancill@debian.org>
License: Apache-2.0

License: Apache-2.0
 On Debian systems, the full text of the Apache-2.0 license
 can be found in the file '/usr/share/common-licenses/Apache-2.0'

License: JDBM
 JDBM LICENSE v1.00
 .
 Redistribution and use of this software and associated documentation
 ("Software"), with or without modification, are permitted provided
 that the following conditions are met:
 .
 1. Redistributions of source code must retain copyright
    statements and notices.  Redistributions must also contain a
    copy of this document.
 .
 2. Redistributions in binary form must reproduce the
    above copyright notice, this list of conditions and the
    following disclaimer in the documentation and/or other
    materials provided with the distribution.
 .
 3. The name "JDBM" must not be used to endorse or promote
    products derived from this Software without prior written
    permission of Cees de Groot.  For written permission,
    please contact cg@cdegroot.com.
 .
 4. Products derived from this Software may not be called "JDBM"
    nor may "JDBM" appear in their names without prior written
    permission of Cees de Groot.
 .
 5. Due credit should be given to the JDBM Project
    (http://jdbm.sourceforge.net/).
 .
 THIS SOFTWARE IS PROVIDED BY THE JDBM PROJECT AND CONTRIBUTORS
 ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT
 NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL
 CEES DE GROOT OR ANY CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.
