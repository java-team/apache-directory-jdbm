/*
 *   Licensed to the Apache Software Foundation (ASF) under one
 *   or more contributor license agreements.  See the NOTICE file
 *   distributed with this work for additional information
 *   regarding copyright ownership.  The ASF licenses this file
 *   to you under the Apache License, Version 2.0 (the
 *   "License"); you may not use this file except in compliance
 *   with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing,
 *   software distributed under the License is distributed on an
 *   "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *   KIND, either express or implied.  See the License for the
 *   specific language governing permissions and limitations
 *   under the License.
 *
 */

package jdbm;


import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;


/**
 * Provides i18n handling of error codes.
 * About formatting see also {@link MessageFormat}
 *
 * @author <a href="mailto:dev@directory.apache.org">Apache Directory Project</a>
 */
public enum I18n
{
    ERR_513("ERR_513"),
    ERR_514("ERR_514"),
    ERR_515("ERR_515"),
    ERR_516("ERR_516"),
    ERR_517("ERR_517"),
    ERR_518("ERR_518"),
    ERR_519("ERR_519"),
    // ERR_520( "ERR_520" ),
    // ERR_521( "ERR_521" ),
    ERR_522("ERR_522"),
    ERR_523("ERR_523"),
    ERR_524("ERR_524"),
    ERR_525("ERR_525"),
    ERR_526("ERR_526"),
    ERR_527("ERR_527"),
    ERR_528("ERR_528"),
    ERR_529("ERR_529"),
    // ERR_530( "ERR_530" ),
    ERR_531("ERR_531"),
    ERR_532("ERR_532"),
    ERR_533("ERR_533"),
    ERR_534("ERR_534"),
    ERR_535("ERR_535"),
    ERR_536("ERR_536"),
    ERR_537("ERR_537"),
    ERR_538("ERR_538"),
    ERR_539_BAD_BLOCK_ID("ERR_539_BAD_BLOCK_ID"),
    ERR_540("ERR_540"),
    ERR_541("ERR_541"),
    ERR_542("ERR_542"),
    ERR_543("ERR_543"),
    ERR_544("ERR_544"),
    ERR_545("ERR_545"),
    ERR_546("ERR_546"),
    ERR_547("ERR_547"),
    ERR_548("ERR_548"),
    ERR_549("ERR_549"),
    ERR_550("ERR_550"),
    ERR_551("ERR_551"),
    ERR_552("ERR_552"),
    ERR_553("ERR_553"),
    ERR_554("ERR_554"),
    ERR_555("ERR_555"),
    ERR_556("ERR_556"),
    ERR_557("ERR_557"),
    ERR_558("ERR_558"),
    ERR_559("ERR_559"),
    ERR_560("ERR_560"),
    ERR_561("ERR_561"),
    ERR_562("ERR_562"),
    ERR_563("ERR_563"),
    ERR_564("ERR_564"),
    ERR_565("ERR_565"),
    ERR_566("ERR_566"),
    ERR_567("ERR_567");

    private static ResourceBundle errBundle = ResourceBundle
        .getBundle( "jdbm.errors" );

    private final static ResourceBundle msgBundle = ResourceBundle
        .getBundle( "jdbm/messages" );

    /** The error code */
    private String errorCode;


    /**
     * Creates a new instance of I18n.
     */
    private I18n( String errorCode )
    {
        this.errorCode = errorCode;
    }


    /**
     * @return the errorCode
     */
    public String getErrorCode()
    {
        return errorCode;
    }


    /**
     * 
     * Translate an error code with argument(s)
     *
     * @param err The error code
     * @param args The argument(s)
     * @return The translate error code
     */
    public static String err( I18n err, Object... args )
    {
        try
        {
            return err + " " + MessageFormat.format( errBundle.getString( err.getErrorCode() ), args );
        }
        catch ( Exception e )
        {
            StringBuffer sb = new StringBuffer();
            boolean comma = false;

            for ( Object obj : args )
            {
                if ( comma )
                {
                    sb.append( "," );
                }
                else
                {
                    comma = true;
                }

                sb.append( obj );
            }

            return err + " (" + sb.toString() + ")";
        }
    }


    /**
     * 
     * Translate a message with argument(s)
     *
     * @param msg The message
     * @param args The argument(s)
     * @return The translated message
     */
    public static String msg( String msg, Object... args )
    {
        try
        {
            return MessageFormat.format( msgBundle.getString( msg ), args );
        }
        catch ( MissingResourceException mre )
        {
            try
            {
                return MessageFormat.format( msg, args );
            }
            catch ( Exception e )
            {
                StringBuffer sb = new StringBuffer();
                boolean comma = false;

                for ( Object obj : args )
                {
                    if ( comma )
                    {
                        sb.append( "," );
                    }
                    else
                    {
                        comma = true;
                    }

                    sb.append( obj );
                }

                return msg + " (" + sb.toString() + ")";
            }
        }
    }
}
    